package flix.dragoncapitaltest;

/**
 * Created by Flix on 10/11/2016.
 */

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface Apiinterface {
    @FormUrlEncoded
    @POST("./")
    Call<Movie> getMovies(@Field("passcode") String passcode);

}
