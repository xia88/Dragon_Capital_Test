package flix.dragoncapitaltest;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.bumptech.glide.Glide;

public class DetailMovie extends AppCompatActivity {
    String IMdbid;
    DBHelper dba;
    Result movie;
    ImageView img;
    CollapsingToolbarLayout collapsingToolbar;
    Typeface tf ;
    EditText Year,Title;
    Button Ok;
    Spinner spinner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_detail_movie);
        tf =  Typeface.createFromAsset(getAssets(), "fonts/museo2.otf");
        collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        spinner = (Spinner) findViewById(R.id.spinner);
        Year = (EditText) findViewById(R.id.input_year);
        Title = (EditText) findViewById(R.id.input_title);
        img = (ImageView) findViewById(R.id.header);
        Intent Retrofit = getIntent();
        Bundle b = Retrofit.getExtras();
        IMdbid = b.getString("IMdbid");
        dba = new DBHelper(DetailMovie.this);
        movie = dba.GetByID(IMdbid);
        Glide.with(this).load(movie.getPoster()).into(img);
        getWindow().setStatusBarColor(Color.TRANSPARENT);
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        collapsingToolbar.setTitle(movie.getTitle());
        collapsingToolbar.setExpandedTitleTypeface(tf);
        collapsingToolbar.setContentScrimColor(getResources().getColor(R.color.colorPrimary));
        collapsingToolbar.setStatusBarScrimColor(getResources().getColor(R.color.colorPrimaryDark));
        collapsingToolbar.setExpandedTitleColor(Color.rgb(255, 255, 255));
        collapsingToolbar.setCollapsedTitleTextColor(Color.rgb(255, 255, 255));
        Title.setText(movie.getTitle());
        Year.setText(movie.getYear());
        Ok = (Button) findViewById(R.id.Ok);
        Ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dba.UpdateByID(IMdbid, spinner.getSelectedItem().toString(),Title.getText().toString(),Year.getText().toString());
                Intent resultIntent = new Intent();
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        });


        String[] type = new String[]{
                "Movie",
                "Series"
        };

        // Initializing an ArrayAdapter
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinneritem,type
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinneritem);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setPopupBackgroundResource(R.color.colorPrimary);
        spinner.setSelection((spinnerArrayAdapter).getPosition(movie.getType()));








    }
}
