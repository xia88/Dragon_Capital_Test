package flix.dragoncapitaltest;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

/**
 * Created by Flix on 10/13/2016.
 */
public class RVAdapter extends RecyclerView.Adapter<RVAdapter.ResultViewHolder>  {
    private Context mContext;
    private List<Result> ResultList;

    public static class ResultViewHolder extends RecyclerView.ViewHolder {
        public TextView title,year;
        public ImageView poster;

        public ResultViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            poster = (ImageView) view.findViewById(R.id.poster);
            year    = (TextView) view.findViewById(R.id.year);
        }
    }

    public RVAdapter(Context mContext, List<Result> resultList) {
        this.mContext = mContext;
        this.ResultList = resultList;
    }
    @Override
    public ResultViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cardview, parent, false);

        return new ResultViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(final ResultViewHolder holder, int position) {
        Result result = ResultList.get(position);
        holder.title.setText(result.getTitle());
        holder.year.setText(result.getYear());
        // loading album cover using Glide library
        Glide.with(mContext).load(result.getPoster()).into(holder.poster);

    }




    @Override
    public int getItemCount() {
        return ResultList.size();
    }
}
