package flix.dragoncapitaltest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

/**
 * Created by Flix on 10/13/2016.
 */
public class DBHelper extends SQLiteOpenHelper {
    private static final String LOG = "DatabaseHelper";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "DragonTest";
    private static final String TABLE_Movie = "TUser";
    private static final String imdbID = "imdbID";
    private static final String Title = "Title";
    private static final String Year = "Year";
    private static final String Type = "Type";
    private static final String Poster = "Poster";


    private static final String CREATE_TABLE_Movie = "CREATE TABLE "
            + TABLE_Movie + "(" + imdbID + " TEXT PRIMARY KEY," + Title
            + " TEXT," + Year + " TEXT," + Type + " TEXT," + Poster
            + " TEXT" + ")";




    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Movie);
        db.execSQL(CREATE_TABLE_Movie);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_Movie);
        // create new tables
        onCreate(db);
    }

    public void createMovieList(List<Result> result) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
       for (int i = 0;i<result.size();i++){
           values.put(imdbID, result.get(i).getImdbID());
           values.put(Title, result.get(i).getTitle());
           values.put(Year, result.get(i).getYear());
           values.put(Type, result.get(i).getType());
           values.put(Poster, result.get(i).getPoster());
           db.insert(TABLE_Movie, null, values);
       }
        db.close();
    }





    public long CheckDataBase() {
        SQLiteDatabase db = this.getReadableDatabase();
        long cnt = DatabaseUtils.queryNumEntries(db, TABLE_Movie);
        db.close();
        return cnt;
    }

    public void ClearAll() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_Movie, null, null);
        db.close();
    }


    public List<Result> getAllMovies() {
        List<Result> resultList = new ArrayList<>();

        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_Movie;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Result result = new Result(cursor.getString(1),cursor.getString(2),cursor.getString(0),cursor.getString(3),cursor.getString(4));
                resultList.add(result);
            } while (cursor.moveToNext());
        }
        db.close();
        // return contact list
        return resultList;
    }






//    String title, String year, String imdbID, String type, String poster
//    values.put(imdbID, result.get(i).getImdbID());
//    values.put(Title, result.get(i).getTitle());
//    values.put(Year, result.get(i).getYear());
//    values.put(Type, result.get(i).getType());
//    values.put(Poster, result.get(i).getPoster());

    public  List<Result> Search(String title) {
        SQLiteDatabase db = this.getReadableDatabase();
        List<Result> resultList = new ArrayList<Result>();
        String selectQuery = "SELECT * FROM "+TABLE_Movie+"\n" +
                "WHERE "+Title+" LIKE '%"+title+"%' ";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor.moveToFirst()) {
            do {
                Result result = new Result(cursor.getString(1),cursor.getString(2),cursor.getString(0),cursor.getString(3),cursor.getString(4));
                resultList.add(result);
            } while (cursor.moveToNext());
        }
        return resultList;
    }


    public  Result GetByID(String imdb) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM "+TABLE_Movie+"\n" +
                "WHERE "+imdbID+"='"+imdb+"'";
        Result result;
        Cursor cursor = db.rawQuery(selectQuery, null);
        cursor.moveToFirst();
        result = new Result(cursor.getString(1),cursor.getString(2),cursor.getString(0),cursor.getString(3),cursor.getString(4));
        return result;
    }

    public void UpdateByID(String Iimdb,String IType, String ITitle, String IYear ){
        SQLiteDatabase db = this.getReadableDatabase();
        String UpdateQuery = " UPDATE " + TABLE_Movie + " Set " + Type + " = '" + IType + "' , " + Title + " = '" + ITitle + "' , " + Year + " = '" + IYear + "' WHERE " + imdbID + " = '"+Iimdb+"'";
        db.execSQL(UpdateQuery);
    }

}
