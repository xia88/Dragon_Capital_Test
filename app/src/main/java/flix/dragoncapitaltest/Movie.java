package flix.dragoncapitaltest;

/**
 * Created by Flix on 10/9/2016.
 */

import java.util.List;


import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Movie {

    @SerializedName("response")
    @Expose
    private boolean response;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("result")
    @Expose
    private List<Result> result = new ArrayList<Result>();

    /**
     * No args constructor for use in serialization
     *
     */
    public Movie() {
    }

    /**
     *
     * @param message
     * @param response
     * @param result
     */
    public Movie(boolean response, String message, List<Result> result) {
        this.response = response;
        this.message = message;
        this.result = result;
    }

    /**
     *
     * @return
     * The response
     */
    public boolean isResponse() {
        return response;
    }

    /**
     *
     * @param response
     * The response
     */
    public void setResponse(boolean response) {
        this.response = response;
    }

    /**
     *
     * @return
     * The message
     */
    public String getMessage() {
        return message;
    }

    /**
     *
     * @param message
     * The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     *
     * @return
     * The result
     */
    public List<Result> getResult() {
        return result;
    }

    /**
     *
     * @param result
     * The result
     */
    public void setResult(List<Result> result) {
        this.result = result;
    }




}
