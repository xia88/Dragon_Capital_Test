package flix.dragoncapitaltest;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.RelativeLayout;
import java.util.Objects;


public class FrontMenu extends AppCompatActivity implements AnimationListener {
    Animation Translate,Alpha,ScrollUp,Left,Right,Fadein,Fadeout,Title;
    CoordinatorLayout Layout;
    Snackbar Notif;
    ImageView Logo1,Logo2;
    Button Circle,Retrofit,About;
    RelativeLayout LogoGroup,viewlayout;
    Typeface tf;
    TextView title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_menu);
        Translate   = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.frontmenutranslate);
        Alpha       = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.frontmenufadein);
        ScrollUp    = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.scrollup);
        Left        = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.left);
        Right       = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.right);
        Fadein      = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadein);
        Fadeout     = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fadeout);
        Title       = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.title);
        Layout      = (CoordinatorLayout) findViewById(R.id.RootView);
        Logo1       = (ImageView) findViewById(R.id.Logo1);
        Logo2       = (ImageView) findViewById(R.id.Logo2);
        LogoGroup   = (RelativeLayout) findViewById(R.id.GroupLogo);
        viewlayout  = (RelativeLayout) findViewById(R.id.view);
        title       = (TextView) findViewById(R.id.title);
        tf = Typeface.createFromAsset(getAssets(), "fonts/museo2.otf");
        Circle      = (Button) findViewById(R.id.Circle);
        Retrofit    = (Button) findViewById(R.id.Retrofit);
//        About       = (Button) findViewById(R.id.About);
        Circle.setTypeface(tf);
        Retrofit.setTypeface(tf);
//        About.setTypeface(tf);
        title.setTypeface(tf);
        Title.setAnimationListener(this);
        Fadein.setAnimationListener(this);
        Fadeout.setAnimationListener(this);
        Left.setAnimationListener(this);
        Right.setAnimationListener(this);
        Translate.setAnimationListener(this);
        Alpha.setAnimationListener(this);
        ScrollUp.setAnimationListener(this);
        title.startAnimation(Title);
        Circle.startAnimation(Left);
        Retrofit.startAnimation(Right);
//        About.startAnimation(Left);
        Logo1.startAnimation(Alpha);

        Circle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FrontMenu.this, Circle.class);
                startActivity(intent);
                finish();
            }
        });

        Retrofit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(FrontMenu.this, Retrofit.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onAnimationEnd(Animation animation) {
        if (animation == Alpha){
            Logo2.setVisibility(View.VISIBLE);
            Logo2.startAnimation(Translate);
        }
        else if (animation == Translate) {
            LogoGroup.startAnimation(ScrollUp);
        }
        else if (animation == Title){
            title.startAnimation(Fadeout);
        }
        else if (animation == Fadeout){
            title.startAnimation(Fadein);
        }
        else if (animation == Fadein){
            title.startAnimation(Fadeout);
        }

    }

    @Override
    public void onAnimationRepeat(Animation animation) {

    }

    @Override
    public void onAnimationStart(Animation animation) {

    }


}
