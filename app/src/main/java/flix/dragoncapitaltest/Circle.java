package flix.dragoncapitaltest;

import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionButton;
import com.oguzdev.circularfloatingactionmenu.library.BuildConfig;
import com.oguzdev.circularfloatingactionmenu.library.FloatingActionMenu;
import com.oguzdev.circularfloatingactionmenu.library.SubActionButton;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;


public class Circle extends AppCompatActivity {
    ImageView CanvasImage;
    public int GRadius,GNCircle = 0;
    Typeface tf;
    EditText SInputCircle,SInputRadius;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_circle);
        CanvasImage = (ImageView) findViewById(R.id.CircleCanvas);
        tf = Typeface.createFromAsset(getAssets(), "fonts/museo2.otf");
        ImageView ImageMenuFab          = new ImageView(this);
        ImageMenuFab.setImageResource(R.drawable.ic_menu_white_24dp);
        ImageView EditNCircleFab        = new ImageView(this);
        EditNCircleFab.setImageResource(R.drawable.ic_group_work_white_24dp);
        ImageView EditRadiusFab         = new ImageView(this);
        EditRadiusFab.setImageResource(R.drawable.ic_rss_feed_white_24dp);
        ImageView CreateFab         = new ImageView(this);
        CreateFab.setImageResource(R.drawable.ic_create_white_24dp);
        FloatingActionButton MenuFab    = new FloatingActionButton.Builder(this).setContentView(ImageMenuFab).setBackgroundDrawable(getResources().getDrawable(R.drawable.fab1)).build();
        SubActionButton.Builder itemBuilder = new SubActionButton.Builder(this);
        TextInputLayout inputLayoutNCirclez  = (TextInputLayout) findViewById(R.id.input_layout_ncircle);
        TextInputLayout inputLayoutRadiusz   = (TextInputLayout) findViewById(R.id.input_layout_radius);
        SubActionButton ButtonEditNCircle   = itemBuilder.setContentView(EditNCircleFab).setBackgroundDrawable(getResources().getDrawable(R.drawable.fab2)).build();
        SubActionButton ButtonEditRadius    = itemBuilder.setContentView(EditRadiusFab).setBackgroundDrawable(getResources().getDrawable(R.drawable.fab3)).build();
        SubActionButton ButtonCreate        = itemBuilder.setContentView(CreateFab).setBackgroundDrawable(getResources().getDrawable(R.drawable.fab1)).build();
        new FloatingActionMenu.Builder(this).addSubActionView(ButtonEditRadius).addSubActionView(ButtonEditNCircle).addSubActionView(ButtonCreate).attachTo(MenuFab).build();

        ButtonCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startpaint();
            }
        });

        ButtonEditNCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Circle.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.circledialog, null);
                final AlertDialog dialog = builder.create();
                dialog.setView(dialogLayout);
                dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.show();
                Button OkCircle = (Button) dialogLayout.findViewById(R.id.Ok);
                Button CancelCircle = (Button) dialogLayout.findViewById(R.id.Cancel);
                final EditText InputCircle = (EditText) dialogLayout.findViewById(R.id.input);
                OkCircle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GNCircle = Integer.parseInt(InputCircle.getText().toString());
                        CircleDraw(GRadius, GNCircle);
                        dialog.cancel();
                    }
                });
                CancelCircle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });
        ButtonEditRadius.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Circle.this);
                LayoutInflater inflater = getLayoutInflater();
                View dialogLayout = inflater.inflate(R.layout.ndialog, null);
                final AlertDialog dialog = builder.create();
                dialog.setView(dialogLayout);
                dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.show();
                Button OkRadius = (Button) dialogLayout.findViewById(R.id.Ok);
                Button CancelRadius = (Button) dialogLayout.findViewById(R.id.Cancel);
                final EditText InputRadius = (EditText) dialogLayout.findViewById(R.id.input);
                OkRadius.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        GRadius = Integer.parseInt(InputRadius.getText().toString());
                        CircleDraw(GRadius, GNCircle);
                        dialog.cancel();
                    }
                });
                CancelRadius.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
            }
        });

        startpaint();
    }


    private void startpaint() {
        AlertDialog.Builder builder = new AlertDialog.Builder(Circle.this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogLayout = inflater.inflate(R.layout.startcircle, null);
        final AlertDialog dialog = builder.create();
        dialog.setView(dialogLayout);
        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        Button OkCircle     = (Button) dialogLayout.findViewById(R.id.Ok);
        SInputCircle        = (EditText) dialogLayout.findViewById(R.id.inputncircle);
        SInputRadius        = (EditText) dialogLayout.findViewById(R.id.inputradius);

        OkCircle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GRadius = Integer.parseInt(SInputRadius.getText().toString());
                GNCircle = Integer.parseInt(SInputCircle.getText().toString());
                CircleDraw(GRadius,GNCircle);
                dialog.cancel();

            }
        });

    }




    private void CircleDraw (int Radius,int NCircle) {
        Bitmap FinalCircle = Bitmap.createBitmap(984, 1749, Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(FinalCircle);
        Paint Parentcircle = new Paint();
        Parentcircle.setColor(getResources().getColor(R.color.colorPrimary));
        Parentcircle.setStyle(Paint.Style.STROKE);
        Parentcircle.setStrokeWidth(Radius / 15);
        canvas.drawCircle(FinalCircle.getWidth() / 2 - 20, FinalCircle.getHeight() / 2 - 20, Radius, Parentcircle);
        Paint Ncircle = new Paint();
        Ncircle.setColor(getResources().getColor(R.color.red));
        Ncircle.setStyle(Paint.Style.FILL);
        Paint Scircle = new Paint();
        Scircle.setColor(getResources().getColor(R.color.green));
        Scircle.setStyle(Paint.Style.FILL);
        double angle = 2 * Math.PI / NCircle;
        for (int i = 0; i < NCircle; i++) {
            Double TempAngle = angle * i - Math.PI / 2;
            Double TempX = FinalCircle.getWidth() / 2 + Radius * Math.cos(TempAngle);
            Double TempY = FinalCircle.getHeight() / 2 + Radius * Math.sin(TempAngle);
            int x = TempX.intValue();
            int y = TempY.intValue();
            if (i == 0) {
                Canvas canvas3 = new Canvas(FinalCircle);
                canvas3.drawCircle(x - 20, y - 20, Radius / 10, Scircle);
            } else {
                Canvas canvas2 = new Canvas(FinalCircle);
                canvas2.drawCircle(x - 20, y - 20, Radius / 10, Ncircle);
            }


        }
        CanvasImage.setImageBitmap(FinalCircle);


    }
}
